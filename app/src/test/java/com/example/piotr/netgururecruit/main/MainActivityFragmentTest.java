package com.example.piotr.netgururecruit.main;

import com.example.piotr.netgururecruit.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by piotr on 03.04.17.
 * <p>
 * NOTE: set $WORKSPACE/android/NetguruRecruit/app before run
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21,
        manifest = "src/main/AndroidManifest.xml", packageName = "com.example.piotr.netgururecruit")
public class MainActivityFragmentTest {
    @Mock
    MainPresenter mPresenter;
    private MainActivity mActivity;

    @Before
    public void setup() {
        mActivity = Robolectric.buildActivity(MainActivity.class).create().get();
        assertNotNull(mActivity);
    }

    @Test
    public void expectWeatherActivityFragmentIsVisibleTrue() throws Exception {
        MainActivityFragment fragment = new MainActivityFragment();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        assertNotNull(fragment.getView());
    }

    @Test
    public void performOnMainActivityFragmentDestroy() throws Exception {
        MainActivityFragment fragment = new MainActivityFragment();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        fragment.onDestroy();
    }

}