package com.example.piotr.netgururecruit.main;

import android.content.Context;

import com.example.piotr.netgururecruit.api.NetworkService;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static junit.framework.Assert.assertNotNull;

/**
 * @author piotr on 03.04.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    @Mock
    Context mContext;
    @Mock
    MainContract.View mMockView;
    private MainPresenter mPresenter;
    private NetworkService mNetworkService;
    private MainInteractor mMainInteractor;

    @BeforeClass
    public static void setupClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(
                __ -> Schedulers.trampoline());
    }

    @Before
    public void setUp() {
        Retrofit retrofit = NetworkServiceHelper.provideRetrofit("http://api.icndb.com");
        assertNotNull(retrofit);

        mNetworkService = retrofit.create(NetworkService.class);
        assertNotNull(mNetworkService);

        mMainInteractor = new MainInteractor(mNetworkService, mContext);
        assertNotNull(mMainInteractor);

        mPresenter = new MainPresenter(mMainInteractor);
        assertNotNull(mPresenter);

        mPresenter.setView(mMockView);
    }

    @Test
    public void expectTestObserverHasNotReceivedAnyOnErrorEvent() throws Exception {
        TestObserver testObserver = new TestObserver();
        provideTestObservable(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
    }

    private void provideTestObservable(TestObserver testObserver) {
        Observable observable = mMainInteractor.provideObservable();
        observable.subscribeWith(testObserver);
    }
}