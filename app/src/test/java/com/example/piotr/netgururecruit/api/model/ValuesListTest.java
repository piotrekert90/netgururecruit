package com.example.piotr.netgururecruit.api.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created by piotr on 03.04.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ValuesListTest {
    private ValuesList mValuesList;
    private static final String TEST_TYPE = "success";
    private static List<Value> categories;

    @Before
    public void setup() {
        categories = new ArrayList<>();
        categories.add(0, new Value(200, "a", null));

        mValuesList = new ValuesList(TEST_TYPE, categories);
    }

    @Test
    public void shouldReturnSuccessType() {
        assertTrue(mValuesList.getType().equals(TEST_TYPE));
    }

    @Test
    public void shouldReturnSavedValue() {
        assertTrue(mValuesList.getValue().equals(categories));
    }

}