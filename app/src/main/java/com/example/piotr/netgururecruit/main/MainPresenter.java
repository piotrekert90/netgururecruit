package com.example.piotr.netgururecruit.main;

import android.support.annotation.VisibleForTesting;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * @author piotr on 16.03.17.
 */
public class MainPresenter implements MainContract.Presenter {
    private final MainInteractor mInteractor;
    private MainContract.View mView;
    private CompositeDisposable mCompositeDisposable;
    @Inject
    MainPresenter(MainInteractor interactor) {
        this.mInteractor = interactor;
    }

    public MainContract.View getView() {
        return mView;
    }

    @Override
    public void setView(MainContract.View view) {
        this.mView = view;
    }

    @Override
    public void provideCompositeDisposable() {
        mCompositeDisposable = new CompositeDisposable();
        mCompositeDisposable.add(provideDisposable());
    }

    @Override
    public void clearCompositeDisposable() {
        mCompositeDisposable.clear();
    }

    private Disposable provideDisposable() {
        return mInteractor.provideObservable().subscribe(mView::handleResponse, mView::handleError);
    }

    @VisibleForTesting
    CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }
}
