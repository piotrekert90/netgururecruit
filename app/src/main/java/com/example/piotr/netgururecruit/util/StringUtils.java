package com.example.piotr.netgururecruit.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * @author piotr on 09.03.17.
 */

public class StringUtils {
    public static Spanned getFormattedJoke(String joke) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(joke, Html.FROM_HTML_MODE_LEGACY);
        }
        //noinspection deprecation
        return Html.fromHtml(joke);
    }
}
