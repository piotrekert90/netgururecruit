package com.example.piotr.netgururecruit.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.piotr.netgururecruit.NetguruApplication;
import com.example.piotr.netgururecruit.R;
import com.example.piotr.netgururecruit.adapter.SimpleDataAdapter;
import com.example.piotr.netgururecruit.api.model.ValuesList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author piotr on 17.08.16.
 */
public class MainActivityFragment extends Fragment implements MainContract.View {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.typeTV)
    TextView connectivityTv;
    @Inject
    MainPresenter mPresenter;
    private Unbinder mUnbinder;
    private SimpleDataAdapter mDataAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void setConnectivityStatus(boolean connectivityChanged) {
        if (connectivityChanged) {
            connectivityTv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark));
        } else {
            connectivityTv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NetguruApplication
                .getApplicationComponent(getActivity().getApplicationContext()).inject(this);

        setPresenter(mPresenter);
        mPresenter.provideCompositeDisposable();

        mUnbinder = ButterKnife.bind(this, view);

        setupRecyclerView(recyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() == null) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
        }
    }

    @Override
    public void handleResponse(ValuesList data) {
        mDataAdapter = mDataAdapter == null ? new SimpleDataAdapter(getContext(), data.getValue()) : mDataAdapter;
        recyclerView.setAdapter(mDataAdapter);
        connectivityTv.setText(data.getType());
        setConnectivityStatus(true);
    }

    @Override
    public void handleError(Throwable e) {
        Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        connectivityTv.setText(e.getMessage());
        setConnectivityStatus(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.clearCompositeDisposable();
        mUnbinder.unbind();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        presenter.setView(this);
    }
}