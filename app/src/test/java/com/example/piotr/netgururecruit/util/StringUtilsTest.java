package com.example.piotr.netgururecruit.util;

import android.text.Spanned;

import com.example.piotr.netgururecruit.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;

/**
 * Created by piotr on 03.04.17.
 * <p>
 * NOTE: set $WORKSPACE/android/NetguruRecruit/app before run
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21,
        manifest = "src/main/AndroidManifest.xml", packageName = "com.example.piotr.netgururecruit")
public class StringUtilsTest {
    private static final String TAG = "StringUtilsTest";
    private StringUtils mStringUtils;

    @Before
    public void setup() {
        mStringUtils = new StringUtils();
    }

    @Test
    public void shouldReturnTheSameString() throws Exception {
        Spanned spanned = mStringUtils.getFormattedJoke(TAG);
        assertEquals(spanned, TAG);
    }

}