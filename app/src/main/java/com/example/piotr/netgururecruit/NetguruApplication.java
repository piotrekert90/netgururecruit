package com.example.piotr.netgururecruit;

import android.app.Application;
import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.example.piotr.netgururecruit.di.ApplicationComponent;
import com.example.piotr.netgururecruit.di.ApplicationModule;
import com.example.piotr.netgururecruit.di.DaggerApplicationComponent;
import com.example.piotr.netgururecruit.di.NetworkServiceModule;

/**
 * @author piotr on 19.08.16.
 */
public class NetguruApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    public static ApplicationComponent getApplicationComponent(Context context) {
        NetguruApplication app = (NetguruApplication) context.getApplicationContext();
        app.mApplicationComponent =
                app.mApplicationComponent == null ? app.createComponent() : app.mApplicationComponent;

        return app.mApplicationComponent;
    }

    @VisibleForTesting
    ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .networkServiceModule(new NetworkServiceModule())
                .build();
    }
}
