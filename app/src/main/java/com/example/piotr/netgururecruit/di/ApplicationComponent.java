package com.example.piotr.netgururecruit.di;

import com.example.piotr.netgururecruit.main.MainActivityFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author piotr on 19.08.16.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkServiceModule.class})
public interface ApplicationComponent {
    void inject(MainActivityFragment fragment);

}
