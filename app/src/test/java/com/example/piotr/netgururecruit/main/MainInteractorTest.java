package com.example.piotr.netgururecruit.main;

import android.content.Context;

import com.example.piotr.netgururecruit.api.NetworkService;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * @author piotr on 03.04.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainInteractorTest {
    private MainInteractor mInteractor;
    @Mock
    Context mContext;
    @Mock
    MainContract.View mMockView;
    private MainPresenter mPresenter;

    @BeforeClass
    public static void setupClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(
                __ -> Schedulers.trampoline());
    }

    @Before
    public void setUp() throws Exception {
        Retrofit retrofit = NetworkServiceHelper.provideRetrofit("http://api.icndb.com");
        assertNotNull(retrofit);

        NetworkService networkService = retrofit.create(NetworkService.class);
        assertNotNull(networkService);

        mInteractor = new MainInteractor(networkService, mContext);
        assertNotNull(mInteractor);

        mPresenter = new MainPresenter(mInteractor);
        assertNotNull(mPresenter);

        mPresenter.setView(mMockView);
    }

    @Test
    public void expectTestObserverWouldReceiveOnlyOneValue() throws Exception {
        TestObserver testObserver = new TestObserver();
        provideTestObservable(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
    }

    private void provideTestObservable(TestObserver testObserver) {
        Observable observable = mInteractor.provideObservable();
        observable.subscribeWith(testObserver);
    }

    @Test
    public void expectWeatherPresenterHasContractedView() {
        assertTrue(!mPresenter.getView().equals(null));
    }

    @Test
    public void expectProvidedCompositeDisposable() throws Exception {
        mPresenter.provideCompositeDisposable();
        assertEquals(mPresenter.getCompositeDisposable().size(), 1);
    }

    @Test
    public void shouldClearCompositeDisposable() throws Exception {
        mPresenter.provideCompositeDisposable();

        mPresenter.clearCompositeDisposable();
        assertFalse(mPresenter.getCompositeDisposable().size() > 0);
    }
}