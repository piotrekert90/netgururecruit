package com.example.piotr.netgururecruit.main;

import com.example.piotr.netgururecruit.BasePresenter;
import com.example.piotr.netgururecruit.BaseView;
import com.example.piotr.netgururecruit.api.model.ValuesList;

/**
 * @author piotr on 19.03.17.
 */

public interface MainContract {

    interface View extends BaseView<MainContract.Presenter> {

        void handleResponse(ValuesList data);

        void handleError(Throwable e);

    }

    interface Presenter extends BasePresenter<MainContract.View> {

        MainContract.View getView();

        void setView(MainContract.View view);

        void provideCompositeDisposable();

        void clearCompositeDisposable();

    }
}
