package com.example.piotr.netgururecruit.api.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by piotr on 03.04.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ValueTest {
    private Value mValue;
    private static final int TEST_ID = 200;
    private static final String TEST_JOKE = "Hello World";
    private static List<String> categories;

    @Before
    public void setup() {
        categories = new ArrayList<>();
        categories.add(0, "Joe");

        mValue = new Value(TEST_ID, TEST_JOKE, categories);
    }

    @Test
    public void expectGetJokeReturnTestIdValue() {
        mValue.getId().equals(TEST_ID);
    }

    @Test
    public void expectGetJokeReturnTestJokeValue() {
        mValue.getJoke().equals(TEST_JOKE);
    }

    @Test
    public void expectGetCategoriesReturnOneCategory() {
        assertEquals(mValue.getCategories().size(), 1);
    }
}