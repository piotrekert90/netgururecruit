package com.example.piotr.netgururecruit.api;


import com.example.piotr.netgururecruit.api.model.ValuesList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * @author piotr on 17.08.16.
 */
public interface NetworkService {

    @GET("/jokes/random/20")
    Observable<ValuesList> getValues();

    @GET("/jokes/random/20")
    Observable<ValuesList> getValuesWithFullName(@Query("firstName") String firstName,
                                                 @Query("lastName") String lastName);
}
