package com.example.piotr.netgururecruit.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.example.piotr.netgururecruit.BuildConfig;
import com.example.piotr.netgururecruit.R;
import com.example.piotr.netgururecruit.main.MainActivity;
import com.example.piotr.netgururecruit.main.MainActivityFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertTrue;

/**
 * Created by piotr on 03.04.17.
 * <p>
 * NOTE: set $WORKSPACE/android/NetguruRecruit/app before run
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21,
        manifest = "src/main/AndroidManifest.xml", packageName = "com.example.piotr.netgururecruit")
public class ActivityUtilsTest {
    private ActivityUtils mActivityUtils;
    private MainActivity mActivity;

    @Before
    public void setup() {
        mActivityUtils = new ActivityUtils();
        mActivity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void shouldReplaceWithWeatherActivityFragment() throws Exception {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        mActivityUtils.replaceFragment(fm, new MainActivityFragment());

        Fragment actualFragment = fm.findFragmentById(R.id.fragment_container);
        assertTrue(actualFragment != null && actualFragment instanceof MainActivityFragment);
    }
}