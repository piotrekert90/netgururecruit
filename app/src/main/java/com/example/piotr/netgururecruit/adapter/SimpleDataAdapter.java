package com.example.piotr.netgururecruit.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.piotr.netgururecruit.R;
import com.example.piotr.netgururecruit.api.model.Value;
import com.example.piotr.netgururecruit.util.StringUtils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author piotr on 17.08.16.
 */
public class SimpleDataAdapter extends RecyclerView.Adapter<SimpleDataAdapter.ViewHolder> {
    private static final String TAG = SimpleDataAdapter.class.getSimpleName();
    private final List<Value> mValues;
    private Context mContext;

    public SimpleDataAdapter(Context context, List<Value> values) {
        this.mContext = context;
        this.mValues = values;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(
                viewGroup.getContext()).inflate(R.layout.recycler_row, viewGroup, false);
        view.setLayoutParams(
                new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.MATCH_PARENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Value item = mValues.get(i);
        Log.d(TAG, "getView: " + item);

        viewHolder.textViewId.setText((Integer.toString(item.getId())));
        viewHolder.textViewJoke.setText(StringUtils.getFormattedJoke(item.getJoke()));
        viewHolder.value.setOnClickListener(v ->
                Snackbar.make(v, mValues.get(i).getId() + " => "
                        + mValues.get(i).getJoke(), Snackbar.LENGTH_LONG).show());
        viewHolder.textViewCategories.setText(Arrays.toString(item.getCategories().toArray()));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.value)
        LinearLayout value;
        @BindView(R.id.valueId)
        TextView textViewId;
        @BindView(R.id.valueJoke)
        TextView textViewJoke;
        @BindView(R.id.valueCategories)
        TextView textViewCategories;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}