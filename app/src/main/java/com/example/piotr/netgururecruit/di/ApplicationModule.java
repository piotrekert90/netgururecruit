package com.example.piotr.netgururecruit.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author piotr on 16.03.17.
 */

@Module
public final class ApplicationModule {

    private final Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
