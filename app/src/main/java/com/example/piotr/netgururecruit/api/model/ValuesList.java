package com.example.piotr.netgururecruit.api.model;

import android.support.annotation.VisibleForTesting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValuesList {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private List<Value> value = null;

    public String getType() {
        return type;
    }

    public List<Value> getValue() {
        return value;
    }

    @VisibleForTesting
    ValuesList(String type, List<Value> value) {
        this.type = type;
        this.value = value;
    }
}