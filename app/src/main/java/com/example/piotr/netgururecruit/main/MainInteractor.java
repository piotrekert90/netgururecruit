package com.example.piotr.netgururecruit.main;

import android.content.Context;

import com.example.piotr.netgururecruit.R;
import com.example.piotr.netgururecruit.api.NetworkService;
import com.example.piotr.netgururecruit.api.model.ValuesList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author piotr on 16.03.17.
 */

@Singleton
class MainInteractor {
    private final NetworkService mNetworkService;
    private final Context mContext;

    @Inject
    MainInteractor(NetworkService networkService, Context context) {
        this.mNetworkService = networkService;
        this.mContext = context;

    }

    Observable<ValuesList> provideObservable() {
        return mNetworkService
                .getValuesWithFullName(
                        mContext.getString(R.string.first_name_default),
                        mContext.getString(R.string.last_name_default))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
