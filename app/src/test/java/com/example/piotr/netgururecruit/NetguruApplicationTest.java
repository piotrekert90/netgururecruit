package com.example.piotr.netgururecruit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by piotr on 03.04.17.
 * <p>
 * NOTE: set $WORKSPACE/android/NetguruRecruit/app before run
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21,
        manifest = "src/main/AndroidManifest.xml", packageName = "com.example.piotr.netgururecruit")
public class NetguruApplicationTest {
    private NetguruApplication mApplication;

    @Before
    public void setup() {
        mApplication = new NetguruApplication();
    }

    @Test
    public void shouldReturnCreatedApplicationComponent() throws Exception {
        assertNotNull(mApplication.createComponent());
        mApplication.getApplicationComponent(mApplication);
    }

    @Test
    public void expectCreatedApplicationComponent() throws Exception {
        assertFalse(mApplication.createComponent().equals(null));
    }

}